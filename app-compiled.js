'use strict';

var _createClass = (function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ('value' in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; })();

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError('Cannot call a class as a function'); } }

var App = (function () {
    function App(start, victim) {
        _classCallCheck(this, App);

        this.refresh(start, victim);
    }

    _createClass(App, [{
        key: 'isNode',
        value: function isNode() {
            return typeof window === 'undefined';
        }
    }, {
        key: 'refresh',

        /*обновляем поля*/
        value: function refresh(start, victim) {
            this.target = victim;
            this.start = start;
        }
    }, {
        key: 'URLparse',

        /*выделяем url*/
        value: function URLparse(str) {
            var t = this;

            if (t.isNode()) {
                if (typeof url === 'undefined') {
                    //а лучше бы задать Ожидаемый Тип
                    url = require('url');
                }
                urlencode = require('urlencode');
                var res = url.parse(str);
                res.href = urlencode.decode(res.href); //декодируем русские названия
                res.pathname = urlencode.decode(res.pathname); //декодируем русские названия
                return res;
            } else {
                var url = document.createElement('a');
                url.href = str;

                var url2 = { // decodeURIComponent не меняло url.href почему-то
                    hostname: url.hostname,
                    pathname: decodeURIComponent(url.pathname),
                    port: url.port,
                    href: decodeURIComponent(url.href)
                };

                return url2;
            }
        }
    }, {
        key: 'extractApiRequest',

        /*берем строку, касающуюся api-запроса из Объекта разобранного URL*/
        value: function extractApiRequest(urlObj) {
            var res = urlObj.pathname.replace('/?api=', '');
            if (res[0] === '/') res = res.substr(1, res.length);
            return res;
        }
    }, {
        key: 'makeAr',

        /*делаем из ввода красивый массив слов*/
        value: function makeAr() {
            //разрежем фразу на слова
            var ar_start = this.start.split(' ');
            var ar_tar = this.target.split(' ');

            //очистим введенные данные от лишних пробелов
            // и до кучи надем длину самого большого слова
            var longest = 0; //наибольшая длина
            for (var i in ar_tar) {
                if (!ar_tar[i].trim()) {
                    delete ar_tar[i];
                } else {
                    if (ar_tar[i].length > longest) {
                        longest = ar_tar[i].length;
                    }
                }
            }

            //а может в пользовательском словосочетании есть длиннее? Проверим
            for (i in ar_start) {
                if (ar_start[i].length > longest) {
                    longest = ar_start[i].length;
                }
            }

            return {
                words: ar_start.concat(ar_tar), // массив слов
                maxL: longest //длина самого большого слова
            };
        }
    }, {
        key: 'format',

        /*делаем строку для вывода из массива слов*/
        value: function format(stringArray) {
            return stringArray.join(' ') + '</br>';
        }
    }, {
        key: 'out',

        /*формируем кусок разметки - реакцию на действие пользователя*/
        value: function out() {
            var t = this;
            var words = t.makeAr();
            var i = 0,
                j = 0;
            var res = t.format(words.words);

            console.log(words.words);
            console.log(words.maxL);

            for (i = words.maxL; i > 1; i--) {
                for (j in words.words) {
                    if (words.words[j].length > 0) {
                        words.words[j] = words.words[j].substring(0, words.words[j].length - 1);
                    }
                }
                res += t.format(words.words);
            }

            return res;
        }
    }]);

    return App;
})();

// =================== ЗАПУСКАЕМ ======================
//создали экземпляр приложения
var iso = new App('', '');

//Если работаем на сревере -
if (iso.isNode()) {

    var http = require('http'),
        fs = require('fs'),
        url = require('url'),
        path = require('path'),

    //appli = require("./app.js"),
    urlencode = require('urlencode'); //https://www.npmjs.com/package/urlencode

    http.createServer(function (req, res) {
        var mimeTypes = {
            '.js': 'text/javascript',
            '.css': 'text/css',
            '.html': 'text/html'
        };
        var api = false,
            textCore = '',
            //текст для замены
        marker = '{MAIN_TEXT}',
            //переменная шаблона
        input_marker = '{INPUT_TEXT}'; //переменная шаблона - содержимое input-а

        //есть ли в запросе информация для Приложения?
        var urlStr = req.url;

        if (urlStr.search('/?api=') > -1) {
            //это запрос к API
            urlStr = urlStr.replace('/?api=', '');
            api = true;
        } else {
            api = false;
        }

        //разберем запрос
        var pathname = url.parse(urlStr).pathname;
        console.log('I see request ' + pathname);

        //запустим Приложение, если просят
        if (api) {
            //var iso = new App('','');
            var want = iso.URLparse(pathname);
            var victim = iso.extractApiRequest(want);

            iso.refresh('Роскомнадзор запретил', victim);
            iso.makeAr();
            textCore = iso.out();
            console.log(textCore);

            pathname = '/';
        }

        if (pathname == '/') {
            pathname = '/index.html';
        }

        pathname = pathname.substring(1, pathname.length); //убираем начальный слэш

        fs.readFile(pathname, 'utf8', function (err, data) {
            if (err) {
                console.log('Could not find of open file for reading...\n');
            } else {
                if (pathname === 'index.html') {
                    //наполняем область текста на Главной
                    data = data.replace(marker, textCore);
                    data = data.replace(input_marker, victim ? victim : '');
                }
                res.writeHead(200, { 'Content-Type': mimeTypes[path.extname(pathname)] });
                res.end(data);
            }
        });
    }).listen(8080);

    console.log('Server is running');
} else {
    $(function () {
        //на кнопку отправки
        $('form .sent').click(function () {

            // поменяем url
            history.pushState({}, document.title, '/?api=' + $(this).siblings('.in').val());

            //получаем URL-запрос, обрабатываем
            var want = iso.URLparse($(this).siblings('.in').val());
            var victim = iso.extractApiRequest(want);

            iso.refresh('Роскомнадзор запретил', victim);
            iso.makeAr();

            //выдаем результат
            $('.out').html(iso.out());

            return false;
        });
    });
}

//# sourceMappingURL=app-compiled.js.map